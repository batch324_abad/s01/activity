<?php 

function getLetterGrade($numericGrade) {
    if ($numericGrade >= 98 && $numericGrade <= 100) {
        return 'A+';
    } elseif ($numericGrade >= 95 && $numericGrade <= 97) {
        return 'A';
    } elseif ($numericGrade >= 92 && $numericGrade <= 94) {
        return 'A-';
    } elseif ($numericGrade >= 89 && $numericGrade <= 91) {
        return 'B+';
    } elseif ($numericGrade >= 86 && $numericGrade <= 88) {
        return 'B';
    } elseif ($numericGrade >= 83 && $numericGrade <= 85) {
        return 'B-';
    } elseif ($numericGrade >= 80 && $numericGrade <= 82) {
        return 'C+';
    } elseif ($numericGrade >= 77 && $numericGrade <= 79) {
        return 'C';
    } elseif ($numericGrade >= 75 && $numericGrade <= 76) {
        return 'C-';
    } elseif ($numericGrade >= 70 && $numericGrade <= 74) {
        return 'D';
    } else {
        return 'F';
    }
}



 ?>