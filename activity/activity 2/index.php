<?php require_once "./code.php"?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Activity 2</title>
</head>
<body>
	<h2>Letter-Based Grading</h2>
	<p><?php echo '87 is equivalent to ' . getLetterGrade(87); ?></p>
	<p><?php echo '94 is equivalent to ' . getLetterGrade(94); ?></p>
	<p><?php echo '74 is equivalent to ' . getLetterGrade(74); ?></p>

</body>
</html>