<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Activity 1</title>
</head>
<body>
	<h3>Full Address</h3>
	<p><?php echo getFullAddress('Philippines', 'Quezon City', 'Metro Manila', '3F Caswynn Bldg., Timog Avenue'); ?></p>
	<p><?php echo getFullAddress('Philippines', 'Makati City', 'Metro Manila', '3F Enzo Bldg., Buendia Avenue'); ?></p>
</body>
</html>